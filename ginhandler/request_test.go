package ginhandler

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/pkg/errors"
	"github.com/stretchr/testify/require"
)

func Test_PrepareRequest(t *testing.T) {
	gin.SetMode(gin.TestMode)

	var actualG *gin.Context
	h := func(g *gin.Context) error {
		actualG = g
		return nil
	}

	g, _ := gin.CreateTestContext(httptest.NewRecorder())

	PrepareRequest(h)(g)

	require.Equal(t, g, actualG)
	require.False(t, g.IsAborted())
	require.Nil(t, g.Errors.Last())
}

func Test_PrepareRequest_error(t *testing.T) {
	gin.SetMode(gin.TestMode)

	expectedErr := errors.New("a-test-error")

	var actualG *gin.Context
	h := func(g *gin.Context) error {
		actualG = g
		return expectedErr
	}

	g, _ := gin.CreateTestContext(httptest.NewRecorder())

	PrepareRequest(h)(g)

	require.Equal(t, g, actualG)
	requireAbortedWithError(t, g, http.StatusBadRequest, expectedErr)
}

func Test_AbortWithStatusCodeOnError(t *testing.T) {
	gin.SetMode(gin.TestMode)

	var actualG *gin.Context
	h := func(g *gin.Context) (status int, err error) {
		actualG = g
		return http.StatusTeapot, nil
	}

	g, _ := gin.CreateTestContext(httptest.NewRecorder())

	AbortWithStatusCodeOnError(h)(g)

	require.Equal(t, g, actualG)
	require.False(t, g.IsAborted())
	require.Nil(t, g.Errors.Last())
}

func Test_AbortWithStatusCodeOnError_error(t *testing.T) {
	gin.SetMode(gin.TestMode)

	expectedErr := errors.New("a-test-error")
	const expectedStatus = http.StatusTeapot

	var actualG *gin.Context
	h := func(g *gin.Context) (status int, err error) {
		actualG = g
		return expectedStatus, expectedErr
	}

	g, _ := gin.CreateTestContext(httptest.NewRecorder())

	AbortWithStatusCodeOnError(h)(g)

	require.Equal(t, g, actualG)
	requireAbortedWithError(t, g, expectedStatus, expectedErr)
}

func Test_WithStatus(t *testing.T) {
	gin.SetMode(gin.TestMode)

	const expectedStatus = http.StatusTeapot

	var actualG *gin.Context
	h := func(g *gin.Context) (status int, err error) {
		actualG = g
		return expectedStatus, nil
	}

	g, _ := gin.CreateTestContext(httptest.NewRecorder())
	WithStatus(h)(g)

	require.Equal(t, g, actualG)
	require.False(t, g.IsAborted())
	require.Equal(t, expectedStatus, g.Writer.Status())
}

func Test_WithStatus_error(t *testing.T) {
	gin.SetMode(gin.TestMode)

	expectedErr := errors.New("a-test-error")
	const expectedStatus = http.StatusTeapot

	var actualG *gin.Context
	h := func(g *gin.Context) (status int, err error) {
		actualG = g
		return expectedStatus, expectedErr
	}

	g, _ := gin.CreateTestContext(httptest.NewRecorder())
	WithStatus(h)(g)

	require.Equal(t, g, actualG)
	requireAbortedWithError(t, g, expectedStatus, expectedErr)
}

func requireAbortedWithError(t *testing.T, g *gin.Context, expectedStatus int, expectedErr error) {
	require.True(t, g.IsAborted())
	require.Equal(t, expectedStatus, g.Writer.Status())
	lastErr := g.Errors.Last()
	require.NotNil(t, lastErr)
	require.Equal(t, expectedErr, lastErr.Err)
	require.False(t, g.Writer.Written())
}
