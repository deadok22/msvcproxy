// Package ginhandler contains reusable handler wrappers that enable simpler request handler testing
package ginhandler

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

// PrepareRequest creates a new gin.HandlerFunc that
// aborts with code 400 on prepare func error
func PrepareRequest(prepare func(*gin.Context) error) gin.HandlerFunc {
	return AbortWithStatusCodeOnError(func(g *gin.Context) (statusCode int, err error) {
		return http.StatusBadRequest, prepare(g)
	})
}

// AbortWithStatusCodeOnError creates a new gin.HandlerFunc that aborts with
// the status code returned by the handle func if it returns a non-nil error
func AbortWithStatusCodeOnError(handle func(*gin.Context) (status int, err error)) gin.HandlerFunc {
	return func(g *gin.Context) {
		status, err := handle(g)
		if err != nil {
			g.Status(status)
			g.Error(err)
			g.Abort()
		}
	}
}

// WithStatus creates a new gin.HandlerFunc that
// responds with the status code returned from the passed handler,
// or aborts with the passed handler's error and status code
func WithStatus(handle func(*gin.Context) (statusCode int, err error)) gin.HandlerFunc {
	return func(g *gin.Context) {
		statusCode, err := handle(g)
		if err != nil {
			g.Status(statusCode)
			g.Error(err)
			g.Abort()
			return
		}
		g.Status(statusCode)
	}
}
