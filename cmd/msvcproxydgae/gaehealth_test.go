package main

import (
	"context"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/pkg/errors"
	"github.com/stretchr/testify/require"
)

func Test_gaeHealthcheckHandler(t *testing.T) {
	checkHealthOK := func(context.Context) error {
		return nil
	}
	requireResponse(t, gaeHealthcheckHandler(checkHealthOK), http.StatusOK, "ok")
}

func Test_gaeHealthcheckHandler_notOK(t *testing.T) {
	checkHealthNotOK := func(context.Context) error {
		return errors.New("a-test-error")
	}
	requireResponse(t, gaeHealthcheckHandler(checkHealthNotOK), http.StatusServiceUnavailable, "not-ok")
}

func requireResponse(t *testing.T, h gin.HandlerFunc, statusCode int, body string) {
	gin.SetMode(gin.TestMode)

	response := httptest.NewRecorder()
	g, _ := gin.CreateTestContext(response)
	g.Request = &http.Request{}

	h(g)

	g.Writer.Flush()

	require.Equal(t, statusCode, response.Code)
	require.Equal(t, body, string(response.Body.Bytes()))
}
