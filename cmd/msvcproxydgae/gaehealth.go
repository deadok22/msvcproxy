package main

import (
	"context"
	"net/http"

	"github.com/gin-gonic/gin"
)

func gaeHealthcheckHandler(checkHealth func(ctx context.Context) error) gin.HandlerFunc {
	return func(g *gin.Context) {
		status := http.StatusOK
		message := "ok"

		err := checkHealth(g.Request.Context())
		if err != nil {
			status = http.StatusServiceUnavailable
			message = "not-ok"
		}

		g.Status(status)
		_, _ = g.Writer.WriteString(message)
	}
}
