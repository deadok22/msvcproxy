package main

import (
	"context"
	"net/http"
	"os"

	"github.com/deadok22/msvcproxy/email"
	"github.com/deadok22/msvcproxy/email/dispatch"
	"github.com/deadok22/msvcproxy/email/mailgun"
	"github.com/deadok22/msvcproxy/email/sendgrid"
	"github.com/deadok22/msvcproxy/web/apiv1"

	"github.com/gin-gonic/gin"
	"github.com/pkg/errors"
)

// msvcproxyd entry point for GAE

func main() {
	sp := dispatch.NewServiceProvider(
		context.Background(),
		sendgridServiceProviderFromEnv(),
		mailgunServiceProviderFromEnv(),
	)
	defer sp.Close()

	g := gin.New()
	apiv1.Install(g.Group("v1"), sp)
	g.GET("/_ah/health", gaeHealthcheckHandler(checkHelathyServiceProviderAvailable(sp)))

	err := http.ListenAndServe(serverAddressFromEnv(), g)
	panic(err)
}

func mailgunServiceProviderFromEnv() email.ServiceProvider {
	return mailgun.NewServiceProvider(
		env("MG_DOMAIN"),
		env("MG_API_KEY"),
		env("MG_PUBLIC_API_KEY"),
	)
}

func sendgridServiceProviderFromEnv() email.ServiceProvider {
	return sendgrid.NewServiceProvider(
		env("SG_SENDGRID_KEY"),
	)
}

func env(key string) string {
	v := os.Getenv(key)
	if v == "" {
		panic("Missing environment variable " + key)
	}
	return v
}

func serverAddressFromEnv() string {
	port := "8080"
	if p := os.Getenv("PORT"); p != "" {
		port = p
	}
	return ":" + port
}

func checkHelathyServiceProviderAvailable(provider *dispatch.ServiceProvider) func(context.Context) error {
	return func(context.Context) error {
		if !provider.HasHealthyServiceProvider() {
			return errors.New("no service providers available")
		}
		return nil
	}
}
