package main

import (
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"os"

	"github.com/deadok22/msvcproxy/email"
	"github.com/deadok22/msvcproxy/email/mailgun"
	"github.com/deadok22/msvcproxy/email/sendgrid"
)

const commandDescription = "Send test emails using an email service providers"

func main() {
	opts, printHelp, err := parseCommandLineOptions(os.Args[0], os.Args[1:]...)
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}

	switch {
	case opts.ShowHelp:
		printHelp(os.Stdout)
	case opts.ShowSampleConfig:
		fmt.Fprintln(os.Stdout, sampleConfigFileText())
	default:
		cfg, err := readConfigFromFile(opts.ConfigFile)
		if err != nil {
			fmt.Fprintln(os.Stderr, "failed to read config file", opts.ConfigFile, "due to error:", err)
			os.Exit(1)
		}

		//TODO add opts for selecting email provider and message details

		sps := serviceProvidersFromConfig(cfg)

		err = sps.SendGrid.Send(context.Background(), testMessage())
		if err != nil {
			fmt.Fprintln(os.Stderr, "sendgrid:", err)
		}

		err = sps.Mailgun.Send(context.Background(), testMessage())
		if err != nil {
			fmt.Fprintln(os.Stderr, "mailgun:", err)
		}
	}
}

func parseCommandLineOptions(
	commandName string,
	args ...string,
) (opts commandLineOptions, printHelp func(to io.Writer), err error) {
	var showHelpQ, showHelpH, showHelpLong bool

	fs := flag.NewFlagSet(commandName, flag.ContinueOnError)
	fs.SetOutput(ioutil.Discard)

	fs.BoolVar(&showHelpQ, "?", false, "Output help message and exit")
	fs.BoolVar(&showHelpH, "h", false, "Output help message and exit")
	fs.BoolVar(&showHelpLong, "help", false, "Output help message and exit")

	fs.StringVar(&opts.ConfigFile, "config", "./config.json", "Configuration file")
	fs.BoolVar(&opts.ShowSampleConfig, "sample-config", false, "Print sample config file")

	err = fs.Parse(args)
	if err == flag.ErrHelp {
		err = nil
	}

	opts.ShowHelp = showHelpQ || showHelpH || showHelpLong

	printHelp = func(w io.Writer) {
		w.Write([]byte("Usage: "))
		w.Write([]byte(commandName))
		w.Write([]byte(" [OPTIONS] \n"))
		w.Write([]byte(commandDescription))
		w.Write([]byte("\n\n"))
		w.Write([]byte("OPTIONS\n"))
		fs.SetOutput(w)
		fs.PrintDefaults()
		fs.SetOutput(ioutil.Discard)
	}

	return opts, printHelp, err
}

type commandLineOptions struct {
	ShowHelp         bool
	ShowSampleConfig bool

	ConfigFile string
}

func readConfigFromFile(file string) (cfg config, err error) {
	configBytes, err := ioutil.ReadFile(file)
	if err != nil {
		return cfg, err
	}
	err = json.Unmarshal(configBytes, &cfg)
	return cfg, err
}

func sampleConfigFileText() string {
	emptyConfigText, err := json.MarshalIndent(config{}, "", "  ")
	if err != nil {
		panic(err)
	}
	return string(emptyConfigText)
}

type config struct {
	Mailgun struct {
		Domain       string `json:"domain"`
		APIKey       string `json:"api-key"`
		PublicAPIKey string `json:"public-api-key"`
	} `json:"mailgun"`

	SendGrid struct {
		Key string `json:"key"`
	} `json:"sendgrid"`
}

func serviceProvidersFromConfig(cfg config) serviceProviders {
	return serviceProviders{
		Mailgun:  mailgun.NewServiceProvider(cfg.Mailgun.Domain, cfg.Mailgun.APIKey, cfg.Mailgun.PublicAPIKey),
		SendGrid: sendgrid.NewServiceProvider(cfg.SendGrid.Key),
	}
}

type serviceProviders struct {
	Mailgun  email.ServiceProvider
	SendGrid email.ServiceProvider
}

func testMessage() *email.Message {
	return &email.Message{
		Sender:     "deadok22@gmail.com",
		Subject:    "sendemail: test message",
		Text:       "Hey there",
		Recipients: []string{"deadok22@gmail.com"},
	}
}
