package email

import (
	"testing"

	"github.com/pkg/errors"
	"github.com/stretchr/testify/require"
)

func TestErrUnavailable(t *testing.T) {
	err := ErrUnavailable(errors.New("a-test-error"))
	require.True(t, IsErrUnavailable(err))
}

func TestErrBadMessage(t *testing.T) {
	err := ErrBadMessage(errors.New("a-test-error"))
	require.True(t, IsErrBadMessage(err))
}

func TestIsErrOfType(t *testing.T) {
	err := newSPError(nil, spErrorTypeBadMessage)
	require.False(t, isErrOfType(err, spErrorTypeUnavailable))
}

func TestIsErrOfTypeHandlesNonSPErrorTypes(t *testing.T) {
	requireHandles := func(err error) {
		require.False(t, isErrOfType(err, spErrorTypeUnavailable))
	}

	requireHandles(nil)
	requireHandles(errors.New("not-a-service-provider-error"))
}
