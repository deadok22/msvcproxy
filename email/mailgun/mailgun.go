package mailgun

import (
	"context"
	"net/http"
	"time"

	"github.com/deadok22/msvcproxy/email"

	"gopkg.in/mailgun/mailgun-go.v1"
)

// NewServiceProvider creates a new email.ServiceProvider implemented using mailgun
func NewServiceProvider(
	domain string,
	apiKey string,
	publicAPIKey string,
) email.ServiceProvider {
	return NewServiceProviderWithCustomHTTPClient(
		newHttpClientWithDefaultTimeout(),
		domain,
		apiKey,
		publicAPIKey,
	)
}

// newHttpClientWithDefaultTimeout creates a new http.Client with request timeout set.
// As mailgun-go.v1 does not offer context-enabled API, using http.Client with no timeout set is unsafe.
func newHttpClientWithDefaultTimeout() *http.Client {
	const requestTimeout = 10 * time.Second
	return &http.Client{
		Timeout: requestTimeout,
	}
}

func NewServiceProviderWithCustomHTTPClient(
	client *http.Client,
	domain string,
	apiKey string,
	publicAPIKey string,
) email.ServiceProvider {
	mg := mailgun.NewMailgun(domain, apiKey, publicAPIKey)
	mg.SetClient(client)
	return newMailgunServiceProvider(mg)
}

func newMailgunServiceProvider(mg mailgunClient) *mailgunServiceProvider {
	return &mailgunServiceProvider{client: mg}
}

//go:generate $GOPATH/bin/mockery -name mailgunClient -testonly -inpkg -note DO_NOT_EDIT

// mailgunClient defines a set of methods of mailgun.Mailgun used here
type mailgunClient interface {
	NewMessage(from, subject, text string, to ...string) *mailgun.Message
	Send(m *mailgun.Message) (status string, messageID string, err error)
}

type mailgunServiceProvider struct {
	client mailgunClient
}

func (p *mailgunServiceProvider) Send(ctx context.Context, message *email.Message) error {
	// TODO respect ctx.Done here
	return p.sendMailgunMessage(p.mailgunMessage(message))
}

func (p *mailgunServiceProvider) sendMailgunMessage(m *mailgun.Message) error {
	_, _, err := p.client.Send(m)
	if err != nil {
		if unexpectedResponse, ok := err.(*mailgun.UnexpectedResponseError); ok {
			switch unexpectedResponse.Actual {
			case http.StatusBadRequest:
				return email.ErrBadMessage(err)
			}
		}
		return email.ErrUnavailable(err)
	}

	return nil
}

func (p *mailgunServiceProvider) mailgunMessage(m *email.Message) *mailgun.Message {
	return p.client.NewMessage(
		m.Sender,
		m.Subject,
		m.Text,
		m.Recipients...,
	)
}
