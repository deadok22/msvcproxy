package mailgun

import (
	"context"
	"net/http"
	"testing"

	"github.com/deadok22/msvcproxy/email"

	"github.com/pkg/errors"
	"github.com/stretchr/testify/require"
	"gopkg.in/mailgun/mailgun-go.v1"
)

func Test_mailgunServiceProvider(t *testing.T) {
	client := &mockMailgunClient{}

	const (
		expectedSender      = "a-test-sender@example.com"
		expectedRecipient1  = "a-test-recipient-one@example.com"
		expectedRecipient2  = "a-test-recipient-two@example.com"
		expectedSubject     = "a-test-subject"
		expectedMessageText = "a-test-email-text"
	)

	expectedMailgunMessage := &mailgun.Message{}

	client.
		On("NewMessage",
			expectedSender,
			expectedSubject,
			expectedMessageText,
			expectedRecipient1,
			expectedRecipient2,
		).
		Return(expectedMailgunMessage).
		Once()
	client.
		On("Send", expectedMailgunMessage).
		Return("sent successfully", "a-message-id", error(nil)).
		Once()

	sp := newMailgunServiceProvider(client)
	err := sp.Send(context.Background(), &email.Message{
		Sender: expectedSender,
		Recipients: []string{
			expectedRecipient1,
			expectedRecipient2,
		},
		Subject: expectedSubject,
		Text:    expectedMessageText,
	})
	require.NoError(t, err)

	client.AssertExpectations(t)
}

func Test_mailgunServiceProvider_badRequest(t *testing.T) {
	client := &mockMailgunClient{}

	expectedMailgunMessage := &mailgun.Message{}
	client.
		On("Send", expectedMailgunMessage).
		Return("bad message", "", &mailgun.UnexpectedResponseError{
			Actual: http.StatusBadRequest,
		}).
		Once()

	sp := newMailgunServiceProvider(client)
	err := sp.sendMailgunMessage(expectedMailgunMessage)
	require.Error(t, err)
	require.True(t, email.IsErrBadMessage(err))

	client.AssertExpectations(t)
}

func Test_mailgunServiceProvider_unavailable(t *testing.T) {
	client := &mockMailgunClient{}

	expectedMailgunMessage := &mailgun.Message{}
	client.
		On("Send", expectedMailgunMessage).
		Return("some error", "", errors.New("a-test-error")).
		Once()

	sp := newMailgunServiceProvider(client)
	err := sp.sendMailgunMessage(expectedMailgunMessage)
	require.Error(t, err)
	require.True(t, email.IsErrUnavailable(err))

	client.AssertExpectations(t)
}
