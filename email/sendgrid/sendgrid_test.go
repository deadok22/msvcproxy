package sendgrid

import (
	"context"
	"io/ioutil"
	"net/http"
	"strings"
	"testing"

	"github.com/deadok22/msvcproxy/email"

	"github.com/pkg/errors"
	sgrest "github.com/sendgrid/rest"
	"github.com/sendgrid/sendgrid-go"
	"github.com/sendgrid/sendgrid-go/helpers/mail"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
)

func Test_sendgridServiceProvider(t *testing.T) {
	client := &mockSendgridClient{}

	testMessage := func() *email.Message {
		return &email.Message{
			Sender:     "a-test-sender@example.com",
			Recipients: []string{"a-test-recipient@example.com"},
			Subject:    "a-test-message",
			Text:       "a-test-text",
		}
	}

	const apiKey = "a-test-api-key"

	expectedContext := context.Background()

	sp := newSendgridServiceProvider(client, apiKey)
	expectedRequest := sp.newSendRequest(expectedContext, testMessage())
	client.
		On(
			"MakeRequest",
			mock.MatchedBy(func(actualRequest *http.Request) bool {
				return assert.Equal(t, expectedRequest.URL, actualRequest.URL) &&
					assert.Equal(t, expectedRequest.Header, actualRequest.Header) &&
					assert.Equal(t, expectedRequest.Context(), actualRequest.Context())
			}),
		).
		Return(
			&http.Response{
				StatusCode: http.StatusAccepted,
				Body:       ioutil.NopCloser(strings.NewReader("")),
			},
			error(nil),
		).
		Once()

	err := sp.Send(expectedContext, testMessage())
	require.NoError(t, err)

	client.AssertExpectations(t)
}

func Test_sendgridServiceProvider_badRequest(t *testing.T) {
	client := &mockSendgridClient{}

	client.
		On("MakeRequest", mock.Anything).
		Return(
			&http.Response{
				StatusCode: http.StatusBadRequest,
				Body:       ioutil.NopCloser(strings.NewReader("")),
			},
			error(nil),
		).
		Once()

	sp := newSendgridServiceProvider(client, "a-test-api-key")
	err := sp.Send(context.Background(), &email.Message{})
	require.Error(t, err)
	require.True(t, email.IsErrBadMessage(err))

	client.AssertExpectations(t)
}

func Test_sendgridServiceProvider_unexpectedStatus(t *testing.T) {
	client := &mockSendgridClient{}

	client.
		On("MakeRequest", mock.Anything).
		Return(
			&http.Response{
				StatusCode: http.StatusServiceUnavailable,
				Body:       ioutil.NopCloser(strings.NewReader("")),
			},
			error(nil),
		).
		Once()

	sp := newSendgridServiceProvider(client, "a-test-api-key")
	err := sp.Send(context.Background(), &email.Message{})
	require.Error(t, err)
	require.True(t, email.IsErrUnavailable(err))

	client.AssertExpectations(t)
}

func Test_sendgridServiceProvider_requestError(t *testing.T) {
	client := &mockSendgridClient{}

	client.
		On("MakeRequest", mock.Anything).
		Return(
			nil,
			errors.New("a-test-error"),
		).
		Once()

	sp := newSendgridServiceProvider(client, "a-test-api-key")
	err := sp.Send(context.Background(), &email.Message{})
	require.Error(t, err)
	require.True(t, email.IsErrUnavailable(err))

	client.AssertExpectations(t)
}

func Test_sendgridServiceProvider_newSendRequest(t *testing.T) {
	testMessage := func() *email.Message {
		return &email.Message{
			Sender:     "a-test-sender@example.com",
			Recipients: []string{"a-test-recipient@example.com"},
			Subject:    "a-test-message",
			Text:       "a-test-text",
		}
	}

	expectedBody := mail.GetRequestBody(sendGridMessage(testMessage()))

	const apiKey = "a-test-api-key"
	r := sendgrid.NewSendClient(apiKey).Request
	r.Body = expectedBody
	expectedRequest, err := sgrest.BuildRequestObject(r)
	require.NoError(t, err)

	expectedCtx, cancel := context.WithCancel(context.Background())
	defer cancel()

	sp := newSendgridServiceProvider(nil, apiKey)
	request := sp.newSendRequest(expectedCtx, testMessage())

	require.Equal(t, expectedRequest.URL, request.URL)
	require.Equal(t, expectedCtx, request.Context())
	require.Equal(t, expectedRequest.Header, request.Header)

	actualBody, err := ioutil.ReadAll(request.Body)
	require.NoError(t, err)
	require.Equal(t, string(expectedBody), string(actualBody))
}

func Test_sendgridMessage(t *testing.T) {
	const (
		expectedSender      = "a-test-sender@example.com"
		expectedRecipient1  = "a-test-recipient-one@example.com"
		expectedRecipient2  = "a-test-recipient-two@example.com"
		expectedSubject     = "a-test-subject"
		expectedMessageText = "a-test-email-text"
	)

	m := sendGridMessage(&email.Message{
		Sender: expectedSender,
		Recipients: []string{
			expectedRecipient1,
			expectedRecipient2,
		},
		Subject: expectedSubject,
		Text:    expectedMessageText,
	})

	require.Equal(t, expectedSender, m.From.Address)

	personalization := m.Personalizations[0]
	require.Equal(t, expectedSubject, personalization.Subject)

	recipients := personalization.To
	require.Equal(t, expectedRecipient1, recipients[0].Address)
	require.Equal(t, expectedRecipient2, recipients[1].Address)

	content := m.Content[0]
	require.Equal(t, contentTypeTextPlain, content.Type)
	require.Equal(t, expectedMessageText, content.Value)
}
