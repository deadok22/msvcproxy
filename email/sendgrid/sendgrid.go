package sendgrid

import (
	"context"
	"net/http"
	"strconv"

	"github.com/deadok22/msvcproxy/email"

	"github.com/pkg/errors"
	sgrest "github.com/sendgrid/rest"
	"github.com/sendgrid/sendgrid-go"
	"github.com/sendgrid/sendgrid-go/helpers/mail"
)

// NewServiceProvider creates a new email.ServiceProvider implemented using sendgrid
func NewServiceProvider(key string) email.ServiceProvider {
	return NewServiceProviderWithCustomHTTPClient(
		http.DefaultClient,
		key,
	)
}

// NewServiceProviderWithCustomHTTPClient creates a new sendgrid email service provider
// with specified http client and API key
func NewServiceProviderWithCustomHTTPClient(client *http.Client, key string) email.ServiceProvider {
	return newSendgridServiceProvider(
		&sgrest.Client{HTTPClient: client},
		key,
	)
}

func newSendgridServiceProvider(client sendgridClient, apiKey string) *sendgridServiceProvider {
	return &sendgridServiceProvider{
		client: client,
		apiKey: apiKey,
	}
}

//go:generate $GOPATH/bin/mockery -name sendgridClient -testonly -inpkg -note DO_NOT_EDIT

// sendgridClient defines a set of methods of sgrest.Client used here
type sendgridClient interface {
	MakeRequest(req *http.Request) (*http.Response, error)
}

type sendgridServiceProvider struct {
	client sendgridClient
	apiKey string
}

// Send message using sendgrid
func (p *sendgridServiceProvider) Send(
	ctx context.Context,
	message *email.Message,
) error {
	r, err := p.sgSend(ctx, message)
	if err != nil {
		if err == context.DeadlineExceeded || err == context.Canceled {
			return err
		}
		return email.ErrUnavailable(err)
	}

	switch r.StatusCode {
	case http.StatusAccepted:
		return nil
	case http.StatusBadRequest:
		return email.ErrBadMessage(errors.New("sendgrid: " + r.Body))
	default:
		return email.ErrUnavailable(errors.New("sendgrid: " +
			strconv.Itoa(r.StatusCode) + " " +
			http.StatusText(r.StatusCode) + "; " +
			"body: " + r.Body,
		))
	}
}

func (p *sendgridServiceProvider) sgSend(ctx context.Context, m *email.Message) (*sgrest.Response, error) {
	resp, err := p.client.MakeRequest(p.newSendRequest(ctx, m))
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	return sgrest.BuildResponse(resp)
}

func (p *sendgridServiceProvider) newSendRequest(ctx context.Context, m *email.Message) *http.Request {
	sgr := sendgrid.NewSendClient(p.apiKey).Request
	sgr.Body = mail.GetRequestBody(sendGridMessage(m))
	req, err := sgrest.BuildRequestObject(sgr)
	if err != nil {
		panic(errors.Wrap(err, "failed to construct sendgrid http request (should never happen)"))
	}
	return req.WithContext(ctx)
}

const contentTypeTextPlain = "text/plain"

func sendGridMessage(m *email.Message) *mail.SGMailV3 {
	return mail.NewV3Mail().
		SetFrom(sendGridEmailAddress(m.Sender)).
		AddPersonalizations(sendGridPersonalization(m)).
		AddContent(mail.NewContent(contentTypeTextPlain, m.Text))
}

func sendGridPersonalization(m *email.Message) *mail.Personalization {
	p := mail.NewPersonalization()
	p.AddTos(sendGridEmailAddresses(m.Recipients...)...)
	p.Subject = m.Subject
	return p
}

func sendGridEmailAddresses(emailAddresses ...string) []*mail.Email {
	emails := make([]*mail.Email, 0, len(emailAddresses))
	for _, emailAddress := range emailAddresses {
		emails = append(emails, sendGridEmailAddress(emailAddress))
	}
	return emails
}

func sendGridEmailAddress(emailAddress string) *mail.Email {
	return &mail.Email{
		Address: emailAddress,
	}
}
