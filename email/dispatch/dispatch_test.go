package dispatch

import (
	"context"
	"testing"

	"github.com/deadok22/msvcproxy/email"
	"github.com/deadok22/msvcproxy/email/emailtest"

	"github.com/pkg/errors"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
)

func TestSendUnavailableWhenAllServiceProvidersAreDown(t *testing.T) {
	unhealthySP1 := newUnstartedMonitoredServiceProvider(nil, nil)
	unhealthySP2 := newUnstartedMonitoredServiceProvider(nil, nil)

	sp := &ServiceProvider{
		providers: []*monitoredServiceProvider{
			unhealthySP1,
			unhealthySP2,
		},
	}

	err := sp.Send(nil, nil)
	require.Error(t, err)
	require.True(t, email.IsErrUnavailable(err))
}

func TestSendPicksAHealthyServiceProvider(t *testing.T) {
	ctx := context.Background()

	expectedMessage := &email.Message{
		Subject:    "a test email subject",
		Recipients: []string{"a-test-recepient@aaaa.bbb", "another-test-recepient@xxx.yyy"},
		Sender:     "a-test-sender@zzz.yyy",
		Text:       "Here's some test email text",
	}

	sp := &emailtest.ServiceProvider{}
	sp.On("Send", ctx, expectedMessage).Return(error(nil)).Once()

	healthySP := newUnstartedMonitoredServiceProvider(ctx, sp)
	healthySP.setHealthy()

	unhealthySP := newUnstartedMonitoredServiceProvider(nil, nil)

	dispatchingSP := &ServiceProvider{
		providers: []*monitoredServiceProvider{
			unhealthySP,
			healthySP,
		},
	}

	err := dispatchingSP.Send(ctx, expectedMessage)
	require.NoError(t, err)

	sp.AssertExpectations(t)
}

func TestHasHealthyServiceProvider(t *testing.T) {
	unhealthySP := newUnstartedMonitoredServiceProvider(nil, &ServiceProvider{})
	unhealthySP.setUnhealthy()

	healthySP := newUnstartedMonitoredServiceProvider(nil, &ServiceProvider{})
	healthySP.setHealthy()

	p := &ServiceProvider{
		providers: []*monitoredServiceProvider{
			unhealthySP,
			healthySP,
			unhealthySP,
		},
	}

	require.True(t, p.HasHealthyServiceProvider())
}

func TestHasHealthyServiceProviderNoProviders(t *testing.T) {
	p := NewServiceProvider(context.Background())
	require.False(t, p.HasHealthyServiceProvider())
}

func TestHasHealthyServiceProviderAllUnhealthy(t *testing.T) {
	unhealthySP := newUnstartedMonitoredServiceProvider(nil, &ServiceProvider{})
	unhealthySP.setUnhealthy()

	p := &ServiceProvider{
		providers: []*monitoredServiceProvider{
			unhealthySP,
			unhealthySP,
		},
	}

	require.False(t, p.HasHealthyServiceProvider())
}

func TestNewServiceProvider(t *testing.T) {
	noopServiceProvider := &ServiceProvider{}
	sp := NewServiceProvider(context.Background(), noopServiceProvider)
	require.NoError(t, sp.Close())
}

func TestMonitoredServiceProviderStopsPeriodicChecksOnContextCancelation(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	noopServiceProvider := &ServiceProvider{}
	msp := newUnstartedMonitoredServiceProvider(ctx, noopServiceProvider)

	monitorStopped := make(chan struct{})
	go func() {
		msp.updateHealthPeriodically()
		close(monitorStopped)
	}()

	cancel()
	<-monitorStopped
}

func TestMonitoredServiceProviderHealthUpdates(t *testing.T) {
	msp := newUnstartedMonitoredServiceProvider(nil, nil)
	require.False(t, msp.Healthy())

	msp.setHealthy()
	require.True(t, msp.Healthy())

	msp.setUnhealthy()
	require.False(t, msp.Healthy())
}

func TestMonitoredServiceProviderUpdateHealthUp(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	sp := &emailtest.ServiceProvider{}
	sp.On("Send", mock.Anything, mock.Anything).Return(error(nil)).Once()

	msp := newUnstartedMonitoredServiceProvider(ctx, sp)
	msp.updateHealth()

	require.True(t, msp.Healthy())

	sp.AssertExpectations(t)
}

func TestMonitoredServiceProviderUpdateHealthDown(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	sp := &emailtest.ServiceProvider{}
	sp.On("Send", mock.Anything, mock.Anything).Return(errors.New("dummy")).Once()

	msp := newUnstartedMonitoredServiceProvider(ctx, sp)
	msp.setHealthy()
	msp.updateHealth()

	require.False(t, msp.Healthy())

	sp.AssertExpectations(t)
}
