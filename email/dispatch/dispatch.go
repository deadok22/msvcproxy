package dispatch

import (
	"context"
	"io"
	"sync/atomic"
	"time"

	"github.com/deadok22/msvcproxy/email"

	"github.com/pkg/errors"
)

// NewServiceProvider creates a new ServiceProvider that
// dispatches messages via one of the healthy providers from 'providers'
func NewServiceProvider(
	ctx context.Context,
	providers ...email.ServiceProvider,
) *ServiceProvider {
	monitorsCtx, cancel := context.WithCancel(ctx)
	return &ServiceProvider{
		stopMonitors: cancel,
		providers:    monitorServiceProviders(monitorsCtx, providers...),
	}
}

// ServiceProvider is an email.ServiceProvider that dispatches messages via
// one of the underlying service provider. Aims to avoid using unhealthy service providers
// when possible.
// Zero value of this type is a no-op email.ServiceProvider.
type ServiceProvider struct {
	stopMonitors context.CancelFunc
	providers    []*monitoredServiceProvider
}

var (
	_ email.ServiceProvider = (*ServiceProvider)(nil)
	_ io.Closer             = (*ServiceProvider)(nil)
)

// Send a message using one of the healthy service providers
func (p *ServiceProvider) Send(ctx context.Context, m *email.Message) error {
	sp := p.aHealthyServiceProvider()
	if sp == nil {
		return email.ErrUnavailable(errors.New("no healthy service provider available"))
	}
	//TODO invalidate the provider's health based on the returned error? This is tricky - what if a 500 is only returned for this message?
	//TODO retry the operation with a different provider on err ?
	return sp.Send(ctx, m)
}

// Close stops monitoring health of service providers
func (p *ServiceProvider) Close() error {
	//TODO consider allowing to wait for termination
	if p.stopMonitors != nil {
		p.stopMonitors()
	}
	return nil
}

// HasHealthyServiceProviders indicates whether there is a healthy service provider.
// A ServiceProvider with no healthy service providers cannot Send successfully.
func (p *ServiceProvider) HasHealthyServiceProvider() bool {
	return p.aHealthyServiceProvider() != nil
}

func (p *ServiceProvider) aHealthyServiceProvider() email.ServiceProvider {
	//TODO consider other strategies here like round-robin, order providers by how long they've stayed healthy, etc
	for _, p := range p.providers {
		if p.Healthy() {
			return p.Provider()
		}
	}
	return nil
}

func monitorServiceProviders(
	monitorsCtx context.Context,
	providers ...email.ServiceProvider,
) []*monitoredServiceProvider {
	monitored := make([]*monitoredServiceProvider, 0, len(providers))
	for _, p := range providers {
		monitored = append(monitored, monitorServiceProvider(monitorsCtx, p))
	}
	return monitored
}

const (
	healthStatusDown int32 = 0
	healthStatusUp   int32 = 1
)

const (
	//TODO make these configurable
	healthCheckTimeout = 5 * time.Second
	healthCheckPeriod  = 30 * time.Minute // This was set to not exceed quota. IRL this should be done way more frequently.
)

func monitorServiceProvider(
	ctx context.Context,
	provider email.ServiceProvider,
) *monitoredServiceProvider {
	sp := newUnstartedMonitoredServiceProvider(ctx, provider)
	sp.Start()
	return sp
}

func newUnstartedMonitoredServiceProvider(
	ctx context.Context,
	provider email.ServiceProvider,
) *monitoredServiceProvider {
	return &monitoredServiceProvider{
		ctx:          ctx,
		provider:     provider,
		healthStatus: healthStatusDown,
	}
}

type monitoredServiceProvider struct {
	ctx          context.Context
	provider     email.ServiceProvider
	healthStatus int32
}

func (p *monitoredServiceProvider) Healthy() bool {
	return atomic.LoadInt32(&p.healthStatus) == healthStatusUp
}

func (p *monitoredServiceProvider) Provider() email.ServiceProvider {
	return p.provider
}

func (p *monitoredServiceProvider) Start() {
	go p.updateHealthPeriodically()
}

func (p *monitoredServiceProvider) updateHealthPeriodically() {
	t := time.NewTicker(healthCheckPeriod)
	defer t.Stop()

	for {
		p.updateHealth()

		select {
		case <-p.ctx.Done():
			//TODO log health checker stop
			return
		case <-t.C:
			continue
		}
	}
}

func (p *monitoredServiceProvider) updateHealth() {
	ctx, cancel := context.WithTimeout(p.ctx, healthCheckTimeout)
	defer cancel()

	//TODO log health check execution

	err := p.performHealthCheck(ctx)
	if err != nil {
		p.setUnhealthy()
	} else {
		p.setHealthy()
	}
}

func (p *monitoredServiceProvider) performHealthCheck(ctx context.Context) error {
	//TODO consider using other means for doing health checks - e.g. calling special endpoint
	return p.provider.Send(ctx, p.messageForHealthCheck())
}

func (p *monitoredServiceProvider) setHealthy() {
	atomic.StoreInt32(&p.healthStatus, healthStatusUp)
}

func (p *monitoredServiceProvider) setUnhealthy() {
	atomic.StoreInt32(&p.healthStatus, healthStatusDown)
}

func (p *monitoredServiceProvider) messageForHealthCheck() *email.Message {
	//TODO make the message configurable - we may want to change addresses, instance identification, etc
	return &email.Message{
		Sender:     "msvcproxy-health-checker@example.com",
		Recipients: []string{"msvcproxy-health-checker@example.com"},
		Subject:    "Health Check at " + time.Now().String(),
		Text:       "health check",
	}
}
