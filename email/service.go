// Package email offers email service provider abstractions
package email

import "context"

//go:generate $GOPATH/bin/mockery -name ServiceProvider -outpkg emailtest -output ./emailtest/ -note DO_NOT_EDIT

// ServiceProvider is an email service provider allowing to send Message
type ServiceProvider interface {
	// Send Message with this ServiceProvider
	Send(context.Context, *Message) error
}

// Message represents an email message
type Message struct {
	Sender string
	Recipients []string
	Subject string
	Text string
}

// IsErrUnavailable is true if the error represents a service provider being unavailable
func IsErrUnavailable(err error) bool {
	return isErrOfType(err, spErrorTypeUnavailable)
}

// IsErrBadMessage if the error represents a service provider not accepting a message
func IsErrBadMessage(err error) bool {
	return isErrOfType(err, spErrorTypeBadMessage)
}

// ErrUnavailable wraps error so that IsErrUnavailable is true for the returned value.
// Meant to be used by ServiceProvider implementations to indicate provider unavailability.
func ErrUnavailable(err error) error {
	return newSPError(err, spErrorTypeUnavailable)
}

// ErrBadMessage wraps error so that IsErrBadMessage is true for the returned value.
// Meant to be used by ServiceProvider implementations to indicate unsupported messages.
func ErrBadMessage(err error) error {
	return newSPError(err, spErrorTypeBadMessage)
}

func newSPError(err error, t spErrorType) *spError {
	return &spError{
		errType: t,
		err:     err,
	}
}

func isErrOfType(err error, t spErrorType) bool {
	spErr, ok := err.(*spError)
	return ok && spErr.errType == t
}

type spErrorType int

const (
	spErrorTypeUnavailable spErrorType = iota
	spErrorTypeBadMessage
)

type spError struct {
	errType spErrorType
	err     error
}

var _ error = (*spError)(nil)

func (err *spError) Error() string {
	return err.err.Error()
}
