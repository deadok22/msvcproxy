package apiv1

import (
	"github.com/gin-gonic/gin"

	"github.com/deadok22/msvcproxy/email"
)

// Install apiv1 endpoints to the root
func Install(
	root gin.IRoutes,
	emailSP email.ServiceProvider,
) {
	root.POST("send", sendHandler(emailSP)...)
}
