package apiv1

import (
	"bytes"
	"context"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/deadok22/msvcproxy/email"
	"github.com/deadok22/msvcproxy/email/emailtest"

	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/json"
	"github.com/pkg/errors"
	"github.com/stretchr/testify/require"
)

func TestPrepareSendRequest(t *testing.T) {
	expectedRequest := SendRequest{
		Subject:    "a test email subject",
		Recipients: []string{"a-test-recepient@aaaa.bbb", "another-test-recepient@xxx.yyy"},
		Sender:     "a-test-sender@zzz.yyy",
		Text:       "Here's some test email text",
	}
	requestBodyBytes, err := json.Marshal(expectedRequest)
	require.NoError(t, err)

	g := &gin.Context{
		Request: httptest.NewRequest(http.MethodPost, "/send", bytes.NewReader(requestBodyBytes)),
	}

	err = prepareSendRequest(g)
	require.NoError(t, err)

	var actualRequest SendRequest
	require.NotPanics(t, func() {
		actualRequest = *mustGetSendRequest(g)
	})

	require.Equal(t, expectedRequest, actualRequest)
}

func TestPrepareRequestErr(t *testing.T) {
	g := &gin.Context{
		Request: httptest.NewRequest(http.MethodPost, "/send", strings.NewReader("invalid-payload")),
	}

	err := prepareSendRequest(g)
	require.Error(t, err)
}

func TestSend(t *testing.T) {
	sendRequest := &SendRequest{
		Subject:    "a test email subject",
		Recipients: []string{"a-test-recepient@aaaa.bbb", "another-test-recepient@xxx.yyy"},
		Sender:     "a-test-sender@zzz.yyy",
		Text:       "Here's some test email text",
	}

	g := ginContextWithBackgroundContext()
	setSendRequest(g, sendRequest)

	sp := &emailtest.ServiceProvider{}
	sp.
		On(
			"Send",
			g.Request.Context(),
			messageFromSendRequest(sendRequest),
		).
		Return(error(nil)).
		Once()

	ep := sendEndpoint{
		emailSP: sp,
	}
	statusCode, err := ep.send(g)
	require.NoError(t, err)
	require.Equal(t, http.StatusAccepted, statusCode)

	sp.AssertExpectations(t)
}

func TestSendErrors(t *testing.T) {
	requireStatusOnErr := func(spErr error, expectedStatus int) {
		sendRequest := &SendRequest{
			Subject:    "a test email subject",
			Recipients: []string{"a-test-recepient@aaaa.bbb", "another-test-recepient@xxx.yyy"},
			Sender:     "a-test-sender@zzz.yyy",
			Text:       "Here's some test email text",
		}

		g := ginContextWithBackgroundContext()
		setSendRequest(g, sendRequest)

		sp := &emailtest.ServiceProvider{}
		sp.
			On(
				"Send",
				g.Request.Context(),
				messageFromSendRequest(sendRequest),
			).
			Return(spErr).
			Once()

		ep := sendEndpoint{
			emailSP: sp,
		}
		statusCode, err := ep.send(g)
		require.Error(t, err)
		require.Equal(t, spErr, err)
		require.Equal(t, expectedStatus, statusCode)

		sp.AssertExpectations(t)
	}

	requireStatusOnErr(email.ErrBadMessage(errors.New("dummy")), http.StatusBadRequest)
	requireStatusOnErr(email.ErrUnavailable(errors.New("dummy")), http.StatusServiceUnavailable)
	requireStatusOnErr(errors.New("dummy"), http.StatusServiceUnavailable)
}

func ginContextWithBackgroundContext() *gin.Context {
	r := &http.Request{}
	return &gin.Context{
		Request: r.WithContext(context.Background()),
	}
}
