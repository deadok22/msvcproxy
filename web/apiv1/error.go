package apiv1

import "github.com/gin-gonic/gin"

// ErrorResponse represents JSON response returned by
// endpoints in this package on errors
type ErrorResponse struct {
	Error string `json:"error"`
}

func errorHandlingMiddleware(g *gin.Context) {
	g.Next()

	if !g.IsAborted() || g.Writer.Written() {
		return
	}

	var response ErrorResponse

	err := g.Errors.Last()
	if err != nil {
		response.Error = err.Error()
	}

	g.JSON(g.Writer.Status(), response)
}
