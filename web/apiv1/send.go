package apiv1

import (
	"net/http"

	"github.com/deadok22/msvcproxy/email"
	"github.com/deadok22/msvcproxy/ginhandler"

	"github.com/gin-gonic/gin"
)

func sendHandler(sp email.ServiceProvider) []gin.HandlerFunc {
	ep := sendEndpoint{
		emailSP: sp,
	}
	return []gin.HandlerFunc{
		errorHandlingMiddleware,
		ginhandler.PrepareRequest(prepareSendRequest),
		ginhandler.WithStatus(ep.send),
	}
}

// SendRequest represents JSON request body for the send endpoint
type SendRequest struct {
	Sender     string   `json:"sender"`
	Recipients []string `json:"recipients"`
	Subject    string   `json:"subject"`
	Text       string   `json:"text"`
}

const sendRequestKey = "apiv1/SendRequest"

func setSendRequest(g *gin.Context, r *SendRequest) {
	g.Set(sendRequestKey, r)
}

func mustGetSendRequest(g *gin.Context) *SendRequest {
	return g.MustGet(sendRequestKey).(*SendRequest)
}

type sendEndpoint struct {
	emailSP email.ServiceProvider
}

func prepareSendRequest(g *gin.Context) error {
	r := &SendRequest{}
	err := g.ShouldBindJSON(r)
	if err != nil {
		return err
	}

	//TODO add extra validation here

	setSendRequest(g, r)

	return nil
}

func (ep *sendEndpoint) send(g *gin.Context) (statusCode int, err error) {
	r := mustGetSendRequest(g)

	err = ep.emailSP.Send(g.Request.Context(), messageFromSendRequest(r))
	if err != nil {
		return statusCodeFromSPError(err), err
	}

	return http.StatusAccepted, nil
}

func messageFromSendRequest(r *SendRequest) *email.Message {
	return &email.Message{
		Sender:     r.Sender,
		Recipients: r.Recipients,
		Subject:    r.Subject,
		Text:       r.Text,
	}
}

func statusCodeFromSPError(err error) int {
	switch {
	case email.IsErrBadMessage(err):
		return http.StatusBadRequest
	default:
		return http.StatusServiceUnavailable
	}
}
