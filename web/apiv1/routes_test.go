package apiv1

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/deadok22/msvcproxy/email/emailtest"

	"github.com/deadok22/msvcproxy/email"
	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
)

func TestAPISend(t *testing.T) {
	sendRequest := SendRequest{
		Sender:     "sender@example.com",
		Recipients: []string{"receipient@example.com"},
		Subject:    "A test subject",
		Text:       "A test message",
	}

	sp := &emailtest.ServiceProvider{}
	sp.
		On("Send", mock.Anything, &email.Message{
			Sender:     sendRequest.Sender,
			Recipients: sendRequest.Recipients,
			Subject:    sendRequest.Subject,
			Text:       sendRequest.Text,
		}).
		Return(error(nil)).
		Once()

	gin.SetMode(gin.TestMode)
	engine := gin.New()
	Install(engine.Group("v1"), sp)

	reqBody, err := json.Marshal(sendRequest)
	require.NoError(t, err)
	req := httptest.NewRequest(http.MethodPost, "/v1/send", bytes.NewReader(reqBody))

	response := httptest.NewRecorder()
	engine.ServeHTTP(response, req)

	require.Equal(t, http.StatusAccepted, response.Code)

	sp.AssertExpectations(t)
}
