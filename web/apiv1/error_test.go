package apiv1

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/pkg/errors"
	"github.com/stretchr/testify/require"
)

func Test_errorHandlingMiddleware(t *testing.T) {
	gin.SetMode(gin.TestMode)

	g, _ := gin.CreateTestContext(httptest.NewRecorder())

	errorHandlingMiddleware(g)

	require.False(t, g.IsAborted())
	require.False(t, g.Writer.Written())
}

func Test_errorHandlingMiddleware_doesNothingIfBodyIsWritten(t *testing.T) {
	gin.SetMode(gin.TestMode)

	const expectedBody = "xxx"

	response := httptest.NewRecorder()
	g, _ := gin.CreateTestContext(response)
	g.Writer.Write([]byte(expectedBody))
	g.Writer.Flush()

	errorHandlingMiddleware(g)

	require.Equal(t, []byte(expectedBody), response.Body.Bytes())
}

func Test_errorHandlingMiddleware_abortedWithNoError(t *testing.T) {
	gin.SetMode(gin.TestMode)

	const expectedStatus = http.StatusTeapot

	expectedBody, err := json.Marshal(ErrorResponse{})
	require.NoError(t, err)

	response := httptest.NewRecorder()
	g, _ := gin.CreateTestContext(response)
	g.Status(expectedStatus)
	g.Abort()

	errorHandlingMiddleware(g)

	g.Writer.Flush()

	require.Equal(t, expectedStatus, response.Code)
	require.Equal(t, expectedBody, response.Body.Bytes())
}

func Test_errorHandlingMiddleware_abortedWithError(t *testing.T) {
	gin.SetMode(gin.TestMode)

	const expectedStatus = http.StatusTeapot

	expectedError := errors.New("a-test-error")
	expectedBody, err := json.Marshal(ErrorResponse{
		Error: expectedError.Error(),
	})
	require.NoError(t, err)

	response := httptest.NewRecorder()
	g, _ := gin.CreateTestContext(response)
	g.Status(expectedStatus)
	g.Error(expectedError)
	g.Abort()

	errorHandlingMiddleware(g)

	g.Writer.Flush()

	require.Equal(t, expectedStatus, response.Code)
	require.Equal(t, expectedBody, response.Body.Bytes())
}
