Must-haves:
    REST APIs
    Tests
    Logging
    Metrics
    Code documentation
    Service documentation
    README.md
        Description of the problem and solution.
        Whether the solution focuses on back-end, front-end or if it's full stack.
        Reasoning behind your technical choices, including architectural.
        Trade-offs you might have made, anything you left out, or what you might do differently if you were to spend additional time on the project.
        Link to other code you're particularly proud of.
        Link to your resume or public profile.
        Link to to the hosted application where applicable.
    Use vendor dir

Open questions:
    Should the service re-attempt delivery via another sp if the first one failed?
        Pros:
            Transparent to clients
                Clients need to implement retry logic in any case, so it seems to be not that big of a win
        Cons:
            Higher complexity
            Higher latency on some requests, especially on multiple sp failures in a row
            Clients cannot implement "at most once" semantics

Nice-to-haves:
    Consider adding feature level based interfaces
        some service providers may not support attachments, for example
    Graceful shutdown
    Service status API
