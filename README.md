## msvcproxy
Mail service proxy.

### Problem Description
A service providing email sending capability that dispatches emails via one of the configured service providers.
Unhealthy service providers are taken out of rotation.

### Solution Description
The solution focuses on the back-end.

The service API consists of a single HTTP endpoint that can be used like this:
```
$ curl -XPOST https://msvcproxy.appspot.com/v1/send -d '
{
    "sender": "test@example.com",
    "recipients": ["@sink.sendgrid.net"],
    "subject": "Hello there",
    "text": "How are you doing today?"
}
'
```
The service responds with status `202` if the message was successfully sent, or with `400` or `504` and a JSON error on errors:
```
$ curl -s -XPOST https://msvcproxy.appspot.com/v1/send -d 'an invalid request'
{"error":"invalid character 'a' looking for beginning of value"}
```

Additionally, service health check endpoint at `/_ah/health` is provided. The health check fails if none of the configured service providers is considered healthy.

The service is hosted at https://msvcproxy.appspot.com (Google App Engine).

#### Architecture
Email service provider implementations are completely separated from the web API layer.
That is done to make sure it's easy to add new service provider implementations and to enable potential service provider code reuse in other projects.

Packages:

![packages diagram](doc/packages.png)

* `cmd/msvcproxydgae` contains the entry point for the service deployed at https://msvcproxy.appspot.com (Google App Engine)
* `web/apiv1` contains REST API related logic
* `email` describes a service provider abstraction
* `email/mailgun`, `email/sendgrid` and `email/dispatch` offer service provider implementations that use Mailgun, SendGrid and multiple service providers, respectively

#### Email Dispatching Logic
A healthy email service provider is chosen for delivering every message on a best-effort basis. See `email/dispatch/dispatch.go`.
Provider selection logic is non-blocking and it will scale well with request count growth.

#### Shortcomings
There is a number of things the solution falls short of.

Here are some of the things I consider to be the most important before the service can be considered production-ready:

* Metrics. Request count, request duration, service status, etc - all with breakdown per service provider
* Logging. Request errors, service provider health updates should be logged.
* Not all of the current service provider implementations are `context.Context`-aware. That makes the service more susceptible to DoS attacks.
* Project dependencies are not copied - the builds are not reproducible.

Also, depending on where the service is deployed and what clients it's meant to serve the following should be considered:

* Authentication.
* Cost attribution / billing.
* Rate limiting (per service-provider, per client, etc).
* Queuing / persisting requests to have the actual sending done asynchronously or by a different service.
